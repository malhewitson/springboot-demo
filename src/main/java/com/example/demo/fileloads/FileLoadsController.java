package com.example.demo.fileloads;

import com.example.demo.model.FileLoadsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 *
 */

@Controller
public class FileLoadsController {

    @Autowired
    private FileLoadsService service;

    @Autowired
    private JPAFileLoadsService JPAservice;

    /**
     * get all file loads
     *
     * @return Result
     */
    @GetMapping("/demo/fileloads")
    @ResponseBody
    public List<FileLoadsDTO> getAllFileLoads() {
        return service.getAllFileLoads();
    }

    /**
     * get all file loads
     *
     * @return Result
     */
    @GetMapping("/demo/jpa/fileloads")
    @ResponseBody
    public List<FileLoadsDTO> getJPAAllFileLoads() {
        return JPAservice.getFileLoads();
    }



    /**
     * get all file loads
     * @param fileLoadsType
     * @return Result
     */
    @GetMapping("/demo/fileload/{fileLoadsType}")
    @ResponseBody
    public List<FileLoadsDTO> getAllFileLoadsOfFileType(@PathVariable String fileLoadsType) {
        return service.getAllFileLoadsOfFileType(fileLoadsType);
    }

    /**
     *
     * @param fileLoadsDTO
     * @return ResponseEntity
     */
    @PostMapping("/demo/fileload")
    public ResponseEntity<Map<String, Long>> postFileLoads(@RequestBody FileLoadsDTO fileLoadsDTO) {

        Long fileLoadId = service.createFileLoads(fileLoadsDTO);

        Map<String, Long> body = Collections.singletonMap("id", fileLoadId);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.LOCATION, "/fileloads/" + fileLoadId );

        return new ResponseEntity<>(body, httpHeaders, HttpStatus.CREATED);
    }

    /**
     *
     * @param fileLoadsDTO
     * @return ResponseEntity
     */
    @PostMapping("/demo/jpa/fileload")
    public ResponseEntity<Map<String, Long>> postJPAFileLoads(@RequestBody FileLoadsDTO fileLoadsDTO) {
        Long id = JPAservice.add(fileLoadsDTO);

        Map<String, Long> body = Collections.singletonMap("id", id);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.LOCATION, "/fileloads/" + id );

        return new ResponseEntity<>(body, httpHeaders, HttpStatus.CREATED);
    }



}
