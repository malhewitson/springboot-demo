package com.example.demo.fileloads;

import com.example.demo.jpa.FileLoadsCRUDRepository;
import com.example.demo.jpa.FileLoadsEntity;
import com.example.demo.model.FileLoadsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class JPAFileLoadsService {

    @Autowired
    FileLoadsCRUDRepository fileLoadsRepository;

    public Long add(FileLoadsDTO dto) {
        FileLoadsEntity entity = fileLoadsRepository.save(toEntity(dto));
        return entity.getId();
    }

    public void delete(long id) {
        fileLoadsRepository.delete(id);
    }
    public List<FileLoadsDTO> getFileLoads() {
        List<FileLoadsEntity> entityList =  (List<FileLoadsEntity>) fileLoadsRepository.findAll();
        return entityList.stream().map(ent -> new FileLoadsDTO(ent)).collect(Collectors.toList());
    }
    public FileLoadsEntity getFileLoadsById(long id) {
        FileLoadsEntity fileLoadsEntity = fileLoadsRepository.findOne(id);
        return fileLoadsEntity;
    }
    private FileLoadsEntity toEntity(FileLoadsDTO fileLoadsDTO) {
        FileLoadsEntity entity = new FileLoadsEntity();
        entity.setFileName( fileLoadsDTO.getFileName());
        entity.setFileType( fileLoadsDTO.getFileType());
        entity.setLoadStatus( fileLoadsDTO.getLoadStatus());
        entity.setS3ArchivePath( fileLoadsDTO.getS3ArchivePath());
        return entity;
    }

}
