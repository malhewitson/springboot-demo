package com.example.demo.fileloads;

import com.example.demo.model.FileLoadsDTO;
import com.example.demo.jdbcrepository.FileLoadsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service to handle REST
 */
@Service
public class FileLoadsService {

    @Autowired
    FileLoadsRepository fileLoadsRepository;

    public List<FileLoadsDTO> getAllFileLoads() {
        return fileLoadsRepository.retrieveFilesToProcess();
    }

    public List<FileLoadsDTO> getAllFileLoadsOfFileType(String fileLoadsType) {
        return fileLoadsRepository.retrieveFilesOfFileType(fileLoadsType);
    }

    public Long createFileLoads(FileLoadsDTO fileLoadsDTO) {
        return fileLoadsRepository.createFileLoads(fileLoadsDTO);

    }
}
