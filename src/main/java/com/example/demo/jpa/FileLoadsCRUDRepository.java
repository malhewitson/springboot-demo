package com.example.demo.jpa;


import com.example.demo.jpa.FileLoadsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileLoadsCRUDRepository extends CrudRepository<FileLoadsEntity, Long> {}
