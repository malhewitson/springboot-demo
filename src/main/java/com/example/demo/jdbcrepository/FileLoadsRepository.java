package com.example.demo.jdbcrepository;

import com.example.demo.model.FileLoadsDTO;
import com.example.demo.model.FileLoadsRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class FileLoadsRepository {

    private static final String GET_ARCHIVED_FILES_FOR_LOADING =
        "SELECT * FROM asgard.file_loads WHERE " +
            " load_status = 'ARCHIVED'" +
            " AND file_type in ('PROD_RANGE','WHOLESALE_PRICES','RRP','MRP','PROMOTIONS');";

    private static final String GET_FILES_OF_TYPE =
        "SELECT * FROM asgard.file_loads WHERE " +
            " file_type = :fileType;";

    private static final String INSERT_FILE_LOADS =
        "INSERT INTO asgard.file_loads (file_name, file_type,  load_status,    s3_archive_path) " +
            "VALUES (:fileName, :fileType,  :loadStatus,    :s3ArchivePath);";


    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<FileLoadsDTO> retrieveFilesToProcess() {
        return jdbcTemplate.query(GET_ARCHIVED_FILES_FOR_LOADING, new FileLoadsRowMapper());
    }

    public List<FileLoadsDTO> retrieveFilesOfFileType(String fileLoadsType) {


        Map<String, Object> params = new HashMap<>();
        params.put("fileType", fileLoadsType);
        return jdbcTemplate.query(GET_FILES_OF_TYPE, params, new FileLoadsRowMapper());
    }

    public Long createFileLoads(FileLoadsDTO fileLoadsDTO) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("fileName", fileLoadsDTO.getFileName());
        params.addValue("fileType", fileLoadsDTO.getFileType());
        params.addValue("loadStatus", fileLoadsDTO.getLoadStatus());
        params.addValue("s3ArchivePath", fileLoadsDTO.getS3ArchivePath());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(INSERT_FILE_LOADS, params, keyHolder);

        return keyHolder.getKey().longValue();
    }
}
