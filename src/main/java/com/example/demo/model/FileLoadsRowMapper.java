package com.example.demo.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Row mapper for converting file_loads table data to
 * DTO object.
 */
public class FileLoadsRowMapper extends AbstractBaseRowMapper {

    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        FileLoadsDTO fileLoadsDTO = new FileLoadsDTO();
        fileLoadsDTO.setId(getLong(rs, "id"));
        fileLoadsDTO.setFileName(getString(rs, "file_name"));
        fileLoadsDTO.setFileType(getString(rs, "file_type"));
        fileLoadsDTO.setErrorFileName(getString(rs, "error_file_name"));
        fileLoadsDTO.setLoadStatus(getString(rs, "load_status"));
        fileLoadsDTO.setProcessedRows(getInteger(rs, "processed_rows"));
        fileLoadsDTO.setErrorRows(getInteger(rs, "error_rows"));
        fileLoadsDTO.setTotalRows(getInteger(rs, "total_rows"));
        fileLoadsDTO.setS3ArchivePath(getString(rs, "s3_archive_path"));
        fileLoadsDTO.setS3Etag(getString(rs, "s3_etag"));

        return fileLoadsDTO;
    }
}
