package com.example.demo.model;

import org.springframework.jdbc.core.RowMapper;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * A base row mapper for converting map to DTO object.
 * @param <T> The type of the value being mapped.
 */
public abstract class AbstractBaseRowMapper<T> implements RowMapper<T> {

    protected Long getLong(ResultSet rs, String column) throws SQLException {
        Long result = rs.getObject(column, Long.class);
        return rs.wasNull() ? null : result;
    }

    protected Integer getInteger(ResultSet rs, String column) throws SQLException {
        Integer result = rs.getObject(column, Integer.class);
        return rs.wasNull() ? null : result;
    }

    protected String getString(ResultSet rs, String column) throws SQLException {
        return rs.getString(column);
    }

    protected BigDecimal getBigDecimal(ResultSet rs, String column) throws SQLException {
        return rs.getBigDecimal(column);
    }

    protected LocalDate getLocalDate(ResultSet rs, String field) throws SQLException {

        Date date = rs.getDate(field);

        if (date == null) {
            return null;
        }

        return date.toLocalDate();
    }
}
