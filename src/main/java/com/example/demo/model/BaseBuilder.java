package com.example.demo.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class BaseBuilder {

    private static final DateTimeFormatter seperatedDtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private static final DateTimeFormatter unseperatedDtf = DateTimeFormatter.ofPattern("yyyyMMdd");

    protected static Integer getInteger(String columnData) {
        try {
            return new Integer(columnData);
        } catch (Exception e) {
            return null;
        }
    }

    protected static Long getLong(String columnData) {
        try {
            return new BigDecimal(columnData).longValueExact();
        } catch (Exception e) {
            return null;
        }
    }

    protected static BigDecimal getBigDecimal(String columnData) {
        try {
            return new BigDecimal(columnData);
        } catch (Exception e) {
            return null;
        }
    }

    public static LocalDate getLocalDate(String columnData) {
        try {
            if (columnData.length() == 8) {
                return LocalDate.parse(columnData, unseperatedDtf);
            }
            return LocalDate.parse(columnData, seperatedDtf);
        } catch (Exception e) {
            return null;
        }
    }
}
